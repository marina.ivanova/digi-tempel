import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.Properties;
import org.apache.commons.io.IOUtils;
import org.digidoc4j.Configuration;
import org.digidoc4j.Configuration.Mode;
import org.digidoc4j.Container;
import org.digidoc4j.ContainerBuilder;
import org.digidoc4j.Signature;
import org.digidoc4j.SignatureBuilder;
import org.digidoc4j.SignatureToken;
import org.digidoc4j.signers.PKCS11SignatureToken;

public class DigiTempel {
  private static final String TOKEN_DRIVER = "token.driver";
  private static final String PROP_TOKEN_SLOT = "token.slot";
  private static final String PROP_TOKEN_PASSWORD = "token.password";


  private static Properties getConf() throws IOException {
    Properties conf = new Properties();
    try (InputStream is = new FileInputStream("conf.properties")) {
      conf.load(is);
    }
    return conf;
  }

  private static SignatureToken initToken() throws IOException {
    Properties conf = getConf();

    String tokenDriver = conf.getProperty(TOKEN_DRIVER);
    int slot = Integer.parseInt(conf.getProperty(PROP_TOKEN_SLOT));
    String password = conf.getProperty(PROP_TOKEN_PASSWORD).trim();

    return new PKCS11SignatureToken(tokenDriver, password.toCharArray(), slot);
  }

  public static void main(String[] args) throws Exception {
    Configuration configuration = new Configuration(Mode.TEST);

    Container container = ContainerBuilder.aContainer()
        .withConfiguration(configuration)
        .withDataFile(IOUtils.toInputStream("test", StandardCharsets.UTF_8), "test.txt", "text/plain")
        .build();

    SignatureToken token = initToken();
    X509Certificate certificate = token.getCertificate();

    Signature signature = SignatureBuilder.aSignature(container).withSignatureToken(token).invokeSigning();

    container.addSignature(signature);
    container.save(new FileOutputStream("test.bdoc"));
    System.out.println("certificate: " + certificate);
  }
}
